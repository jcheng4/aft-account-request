module "sandbox-ai" {
  source = "./modules/aft-account-request"

  control_tower_parameters = {
    AccountEmail              = "mustafa.onan+sandbox-ai@orioninc.com"
    AccountName               = "sandbox-ai"
    ManagedOrganizationalUnit = "Sandbox"
    SSOUserEmail              = "mustafa.onan+sandbox-ai@orioninc.com"
    SSOUserFirstName          = "Mustafa Cem"
    SSOUserLastName           = "Onan"
  }

  account_tags = {
    "Type" = "Sandbox"
    "Purpose" = "AI"
  }

  change_management_parameters = {
    change_requested_by = "mustafa.onan+sandbox-ai@orioninc.com"
    change_reason       = "Initial account creation"
  }

  custom_fields = {
    group = "Sandbox"
  }

  account_customizations_name = "sandbox-account-customizations"
}

module "jcheng_sandbox-temp1" {
  source = "./modules/aft-account-request"

  control_tower_parameters = {
    AccountEmail              = "jcheng+sandbox-temp1@natera.com"
    AccountName               = "sandbox-temp1"
    ManagedOrganizationalUnit = "Sandbox"
    SSOUserEmail              = "jcheng+sandbox-temp1@natera.com"
    SSOUserFirstName          = "John"
    SSOUserLastName           = "Cheng"
  }

  account_tags = {
    "Type" = "Sandbox"
    "Purpose" = "AI"
  }

  change_management_parameters = {
    change_requested_by = "jcheng@natera.com"
    change_reason       = "Initial account creation"
  }

  custom_fields = {
    group = "Sandbox"
  }

  account_customizations_name = "sandbox-account-customizations"
}
